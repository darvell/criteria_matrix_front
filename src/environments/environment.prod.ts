export const environment = {
  production: true,
  apiEndpoint: 'https://matrix.darvell.ru/api',
  clientEndpoint: 'https://matrix.darvell.ru'
};
