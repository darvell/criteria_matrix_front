import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginPageComponent} from './components/auth/login_page.component';
import {MainPageComponent} from './components/main_page/mainpage.component';
import {ViewAllMatrixComponent} from './components/matrix/viewall/viewall_matrix.component';
import {AddNewMatrixComponent} from './components/matrix/addnew/addnew_matrix.component';
import {EditMatrixComponent} from './components/matrix/edit/edit_matrix.component';
import {JoinOprosComponent} from './components/opros/join/join_opros.component';
import {ViewResultMatrixComponent} from './components/matrix/view_result_matrix/view_result_matrix.component';
import {RegisterPageComponent} from './components/register/register.component';
import {JoinMergedOprosComponent} from './components/opros/join/join_merged_opros.component';
import {TestAdminComponent} from './components/testing/admin_page/testadmin.component';
import {TestEditComponent} from './components/testing/admin_page/test_edit/testedit.component';
import {QuestionEditComponent} from './components/testing/admin_page/question_edit/questionedit.component';
import {TestUserComponent} from './components/testing/user_page/testuser.component';
import {ActiveTestsComponent} from './components/testing/user_page/active_test_list/testlist.component';
import {TestingMainComponent} from './components/testing/user_page/main/testingmain.component';
import {CompleteTestPageComponent} from './components/testing/user_page/complete_test_page/complete_test_page.component';


const routes: Routes = [
  {path: '', redirectTo: 'app/viewall', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent},
  {path: 'register', component: RegisterPageComponent},
  {
    path: 'opros', children: [
      {path: 'join/:oprosId', component: JoinOprosComponent},
      {path: 'process/:interviewId', component: JoinOprosComponent},
      {path: 'join_merged/:oprosId', component: JoinMergedOprosComponent},
    ]
  },
  {path: 'test_user', component: TestingMainComponent, children:[
      {path: 'viewall', component: ActiveTestsComponent},
      {path: 'process', component: TestUserComponent},
      {path: 'complete/:testProcessId', component: CompleteTestPageComponent},
    ]},
  {
    path: 'app', component: MainPageComponent, children: [
      {path: 'viewall', component: ViewAllMatrixComponent},
      {path: 'add', component: AddNewMatrixComponent},
      {path: 'viewMatrix/:id', component: EditMatrixComponent},
      {path: 'viewResult/:oprosId', component: ViewResultMatrixComponent},
      {path: 'test_admin', component: TestAdminComponent},
      {path: 'test_admin/edit/:testId', component: TestEditComponent},
      {path: 'test_admin/add', component: TestEditComponent},
      {path: 'test_admin/question', component: QuestionEditComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
