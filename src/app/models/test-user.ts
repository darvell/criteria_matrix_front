export interface TestProcess {
  id: number;
}

export interface Answer {
  id: number;
  text: string;
  checked: boolean;
}

export interface Question {
  id: number;
  text: string;
  answers: Answer[];
  completed: boolean;
}

export interface TestToDo {
  id: number;
  questions: Question[];

}

export interface ActiveTestProcess {
  id: number;
  startDate: Date;
  complete: boolean;
}

export class TestInAction {
  testId: number;
  testName: string;
  complete: boolean;
  testProcessList: ActiveTestProcess[];
}
