export interface SubtitleActionData {
  title: string;
  actionId: number;
}
