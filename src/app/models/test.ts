import {Test} from 'tslint';

export interface Test {
  id: number;
  name: string;
  description: string;
}

export class TestImpl implements Test {
  description: string;
  id: number;
  name: string;
}

export interface TestsResponse {
  tests: Test[];
}


export interface QuestionRA {
  question: TestQuestion;
  answers: QuestionAnswer[];
}

export interface TestQuestion {
  id: number;
  text: string;
}

export interface QuestionAnswer {
  id: number;
  answerText: string;
  right: boolean;
}
