export interface CriteriaMatrix {
  id: number;
  maxMembersCount: number;
  useCoefficient: boolean;
  name: string;
  criteriaSet: Criteria[];
  matrixEntities: MatrixEntity[];
  oprosList: Opros[];
  deleted: boolean;
}

export interface Criteria {
  id: number;
  value: string;
  weight: number;
}

export interface MatrixEntity {
  id: number;
  value: string;
}

export interface EntityGroup {
  id: number;
  name: string;
  entityIds: number[];
}

export interface Opros {
  id: number;
  createDate: Date;
  open: boolean;
  intervieweesCount: number;

}


export interface ResultMatrixResp {
  resultMatrix: { [name: number]: { [name2: number]: number } };
  criteriaLegend: { [name: number]: string };
  entityLegend: { [name: number]: string };
  weights: { [name: number]: string };
}

export interface CreateMatrixRequest {
  name: string;
  useCoefficient: boolean;
  entityList: MatrixEntity[];
  criteriaList: Criteria[];
}
