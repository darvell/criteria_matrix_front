import {Criteria, EntityGroup, MatrixEntity} from './matrix';

export interface JoinOprosResponse {
  success: boolean;
  interviewId: number;
  oprosId: number;
  merged: boolean;
}

export interface MergedInterview {
  interviews: Interview[];
}

export interface Interview {
  interviewId: number;
  criterias: Criteria[];
  entitys: MatrixEntity[];
  entityGroups: EntityGroup[];
  needMax: boolean;
  needMin: boolean;
  partAnswer: boolean;
  maxRaiting: number;
  actionText: string;
  maxAnswersCount: number;
  custom: number;
}


export class OprosAnswer {
  entityId: number;
  answer: number;
  entityName: string;
  disabled = false;
  error = false;
  groupId = -1;

  constructor(entity: MatrixEntity) {
    this.entityId = entity.id;
    this.answer = 0;
    this.entityName = entity.value;
  }
}

