import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {StoreService} from '../../services/store.service';
import {version} from '../../../../package.json';

@Component({
  selector: 'app-main-page',
  templateUrl: './mainpage.component.html',
  styleUrls: ['mainpage.component.scss']
})
export class MainPageComponent implements OnInit {

  constructor(private authService: AuthService,
              private storeService: StoreService,
              private router: Router) {
  }

  title: string;
  icon: string;

  ngOnInit(): void {
    console.log(version);
    const token = this.storeService.getToken();
    if (token == null) {
      this.router.navigate(['login']);
    }
    this.authService.checkToken().subscribe(resp => {
        this.storeService.saveToken(resp.token);
        if (this.storeService.isLastServiceTesting()) {
          this.changeToTesting();
        } else {
          this.changeToMatrix();
        }
      }
      , err => {
        console.error(err);
        this.router.navigate(['login']);
      });
  }

  changeToMatrix() {
    this.title = 'Матрица критериев';
    this.icon = 'calendar_view_month';
    this.router.navigate(['app/viewall']);
  }

  changeToTesting() {
    this.title = 'Тестирование';
    this.icon = 'grading';
    this.router.navigate(['app/test_admin']);
  }

  public logout() {
    this.storeService.deleteToken();
    this.router.navigate(['login']);
  }
}
