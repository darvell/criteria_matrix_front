import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {StoreService} from '../../../services/store.service';
import {TestAdminService} from '../../../services/test_admin.service';
import {Test} from '../../../models/test';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {SubtitlePageComponent} from '../../main/page-with-subtitle/subtitle-page.component';

@Component({
  selector: 'app-test-admin',
  templateUrl: 'testadmin.component.html',
  styleUrls: ['testadmin.component.scss']
})
export class TestAdminComponent extends SubtitlePageComponent implements OnInit {

  tests: Test[];
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['id', 'name', 'description', 'actions'];

  constructor(public snackBar: MatSnackBar,
              public storeService: StoreService,
              public testAdminService: TestAdminService,
              private router: Router) {
    super();
    this.subtitleActions.push({title: 'Добавить', actionId: 0});
  }

  onSubtitleAction(actionId: number) {
    switch (actionId) {
      case 0: {
        this.addNewTest();
        break;
      }
    }
  }

  ngOnInit(): void {
    this.storeService.changeLastToTesting();
    this.testAdminService.getAllTests().subscribe(
      resp => {
        this.tests = resp.tests;
        this.dataSource = new MatTableDataSource<Test>(this.tests);
      }
    );
  }

  showAndEditTest(testId: number) {
    this.router.navigate(['/app/test_admin/edit/', testId]);
  }

  addNewTest() {
    this.router.navigate(['/app/test_admin/add']);
  }
}
