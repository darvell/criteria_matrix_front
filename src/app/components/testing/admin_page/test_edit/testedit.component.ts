import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TestAdminService} from '../../../../services/test_admin.service';
import {Test, TestImpl, TestQuestion} from '../../../../models/test';
import {FormControl, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Clipboard} from '@angular/cdk/clipboard';
import {environment} from '../../../../../environments/environment';

export const SAVE_MESSAGE = 'Успешно сохранено';
export const DELETE_MESSAGE = 'Тест удалён';



@Component({
  selector: 'app-test-edit',
  templateUrl: 'testedit.component.html',
  styleUrls: ['testedit.component.scss']
})
export class TestEditComponent implements OnInit {

  @ViewChild('saveButton') saveButton: ElementRef;

  public testId: number;
  public test: Test;
  public questions: TestQuestion[];

  public isEditButtonsDisabled = true;
  public isEditQuestionsDisabled = true;

  testEditFG = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
  });

  constructor(public route: ActivatedRoute,
              public testAdminService: TestAdminService,
              public router: Router,
              public snackBar: MatSnackBar,
              private clipboard: Clipboard,) {

  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.testId = Number(params.get('testId'));
      if (this.testId) {
        this.testAdminService.getTestById(this.testId).subscribe(resp => {
            this.test = resp;
            this.loadQuestions(this.testId);
            this.prepareForm(this.test);
          }
        );
      } else {
        this.test = new TestImpl();
        this.prepareForm(this.test);
      }
    });
  }

  prepareForm(test: Test) {
    this.resetFromGroup(test);
    this.testEditFG.valueChanges.subscribe(value => {
      this.isEditButtonsDisabled = false;
    });
  }

  resetFromGroup(test: Test) {
    this.testEditFG.get('name').setValue(test.name);
    this.testEditFG.get('description').setValue(test.description);

  }

  backToAll() {
    this.router.navigate(['/app/test_admin']);
  }

  saveTestInfo() {
    if (this.testEditFG.valid) {
      if (this.testId !== 0) {
        this.testAdminService.updateTest(this.testId, this.testEditFG.value)
          .subscribe(resp => {
            this.showShackBar(SAVE_MESSAGE);
            this.isEditButtonsDisabled = true;
          });
      } else {
        this.testAdminService.addTest(this.testEditFG.value).subscribe(resp => {
          this.showShackBar(SAVE_MESSAGE);
          this.isEditButtonsDisabled = true;
          this.isEditQuestionsDisabled = false;
          this.testId = resp.id;
        });
      }
    }
  }

  addQuestion() {
    this.router.navigate(['/app/test_admin/question'], {queryParams: {testId: this.testId}});
  }

  cancelEditing() {
    this.resetFromGroup(this.test);
    this.isEditButtonsDisabled = true;
  }

  showShackBar(message: string) {
    this.snackBar.open(message, 'ОК', {duration: 3000});
  }

  loadQuestions(testId: number) {
    this.isEditQuestionsDisabled = false;
    this.testAdminService.getQuestionsByTest(testId).subscribe(
      value => this.questions = value
    );
  }

  dropQuestion(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.questions, event.previousIndex, event.currentIndex);
  }

  openQuestion(qId: number) {
    this.router.navigate(['/app/test_admin/question'], {queryParams: {testId: this.testId, questionId: qId}});
  }

  deleteTest() {
    if (this.testId) {
      this.testAdminService.deleteTest(this.testId).subscribe(resp => {
        this.showShackBar(DELETE_MESSAGE);
        this.backToAll();
      });
    } else {
      this.showShackBar(DELETE_MESSAGE);
      this.backToAll();
    }
  }

  public copyLink() {
    this.clipboard.copy(`${environment.clientEndpoint}/test_user/viewall?testId=${this.testId}&action=join`);
    this.snackBar.open('Ссылка скопирована', 'Ок', {duration: 3000});

  }
}
