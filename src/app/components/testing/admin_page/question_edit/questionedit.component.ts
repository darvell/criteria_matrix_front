import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TestAdminService} from '../../../../services/test_admin.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {QuestionAnswer, QuestionRA} from '../../../../models/test';

export const SAVE_MESSAGE = 'Успешно сохранено';
export const DELETE_MESSAGE = 'Ответ удалён';

@Component({
  selector: 'app-question-edit',
  templateUrl: 'questionedit.component.html',
  styleUrls: ['questionedit.component.scss']
})
export class QuestionEditComponent implements OnInit {


  testId: number;
  questionId: number;

  questionRA: QuestionRA;

  questionEditFG = new FormGroup({
    question: new FormGroup({
      text: new FormControl(''),
    }),
    answers: new FormArray([])
  });

  public isEditButtonsDisabled = true;


  constructor(public route: ActivatedRoute,
              public testAdminService: TestAdminService,
              public router: Router,
              public snackBar: MatSnackBar) {

    this.questionEditFG.valueChanges.subscribe(() => {
        this.isEditButtonsDisabled = false;
      }
    );
  }

  get questionFG(): FormGroup {
    return this.questionEditFG.get('question') as FormGroup;
  }

  get answers(): FormArray {
    return this.questionEditFG.get('answers') as FormArray;
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.testId = Number(params.get('testId'));
      this.questionId = Number(params.get('questionId'));
      this.loadQuestion();
    });
  }

  loadQuestion() {
    this.testAdminService.getQuestionById(this.questionId).subscribe(resp => {
      this.questionRA = resp;
      this.refillForm();
    });
  }

  addEmptyAnswerControl() {
    this.answers.push(
      new FormGroup({
        id: new FormControl(),
        answerText: new FormControl('', Validators.required),
        right: new FormControl(false)
      })
    );
  }

  addAnswer(answer: QuestionAnswer) {
    this.answers.push(
      new FormGroup({
        id: new FormControl(answer.id),
        answerText: new FormControl(answer.answerText, Validators.required),
        right: new FormControl(answer.right)
      })
    );
    // console.log(this.answers);
    // this.refillForm();
    // window.scrollTo(0, document.body.scrollHeight);
    // console.log(document.body.scrollHeight);
  }

  saveQuestion() {
    if (this.questionEditFG.valid) {
      if (this.questionId) {
        this.testAdminService.updateQuestion(this.questionId, this.questionEditFG.value).subscribe(resp => {
          this.isEditButtonsDisabled = true;
          this.questionRA = resp;
          this.refillForm();
          this.showShackBar(SAVE_MESSAGE);
        });
      } else {
        this.testAdminService.addQuestion(this.testId, this.questionEditFG.value).subscribe(resp => {
          this.questionRA = resp;
          this.questionId = this.questionRA.question.id;
          this.refillForm();
          this.isEditButtonsDisabled = true;
          this.showShackBar(SAVE_MESSAGE);
        });
      }
    }
  }

  showShackBar(message: string) {
    this.snackBar.open(message, 'ОК', {duration: 3000});
  }

  refillForm() {
    this.questionEditFG.reset();
    this.answers.clear();
    if (this.questionRA) {
      this.questionEditFG.get('question').get('text').setValue(this.questionRA.question.text);
      this.questionRA.answers.forEach(a => this.addAnswer(a));
    }
    this.isEditButtonsDisabled = true;
  }

  deleteAnswer(answerPosition: number) {
    this.testAdminService.deleteAnswer(this.questionRA.answers[answerPosition].id).subscribe(resp => {
      this.showShackBar(DELETE_MESSAGE);
      this.loadQuestion();
    });
  }

  backToTest() {
    this.router.navigate(['/app/test_admin/edit', this.testId]);
  }


}
