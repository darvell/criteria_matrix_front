import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TestUserService} from '../../../services/test_user.service';
import {ActiveTestProcess, Answer, TestToDo} from '../../../models/test-user';

@Component({
  selector: 'app-test-user',
  templateUrl: 'testuser.component.html',
  styleUrls: ['testuser.component.scss']
})
export class TestUserComponent implements OnInit {

  testProcessId: number;
  currQuestionIndex = 0;

  testToDo: TestToDo;
  testPrepared = false;

  constructor(public route: ActivatedRoute,
              public router: Router,
              public snackBar: MatSnackBar,
              public testUserService: TestUserService) {
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      const id = Number(params.get('id'));
      const action = params.get('action');
      if (action === 'new') {
        this.testUserService.joinTest(id).subscribe(resp => {
          this.testProcessId = resp.id;
          this.getTest();
        });
      } else {
        this.testProcessId = id;
        this.getTest();
      }
    });
  }

  getTest() {
    this.testUserService.getTest(this.testProcessId).subscribe(resp => {
      this.testToDo = resp;
      if (this.testToDo.questions.length > 0) {
        this.testPrepared = true;
        this.currQuestionIndex = 0;
      } else {
        this.doFinishTestActions();
      }
    });
  }

  selectQuestion(index: number) {
    this.currQuestionIndex = index;
  }

  nextQuestion(): boolean {
    if (this.currQuestionIndex + 1 < this.testToDo.questions.length) {
      this.currQuestionIndex++;
      return true;
    }
    return false;
  }

  getCurrAnswerById(anwerId: number) {
    return this.testToDo.questions[this.currQuestionIndex].answers
      .find(value => value.id === anwerId);
  }

  chooseAnswer(anwerId: number) {
    this.getCurrAnswerById(anwerId).checked = !this.getCurrAnswerById(anwerId).checked;
  }

  saveAnswer() {
    const answerList = this.testToDo.questions[this.currQuestionIndex].answers.filter(a => a.checked).map(a => a.id);
    this.testUserService.addAnswer(this.testProcessId
      , this.testToDo.questions[this.currQuestionIndex].id
      , answerList
    ).subscribe(resp => {
      this.testToDo.questions[this.currQuestionIndex].completed = true;
      if (!this.nextQuestion()){
        this.doFinishTestActions();
      }
    });
  }

  doFinishTestActions() {
    this.router.navigate(['test_user/complete', this.testProcessId]);
  }
}
