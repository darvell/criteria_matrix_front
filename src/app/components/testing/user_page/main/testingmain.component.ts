import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../../../../services/auth.service';
import {StoreService} from '../../../../services/store.service';

@Component({
  selector: 'app-testing-main',
  templateUrl: 'testingmain.component.html',
  styleUrls: ['testingmain.component.scss']
})

export class TestingMainComponent implements OnInit {


  constructor(public route: ActivatedRoute,
              public router: Router,
              public snackBar: MatSnackBar,
              public authService: AuthService,
              private storeService: StoreService) {
  }


  ngOnInit(): void {

    const token = this.storeService.getToken();

    if (!token) {
      this.router.navigate(['login'], {queryParams: {returnUrl: this.router.url}});
    } else {

      this.authService.checkToken().subscribe(resp => {
          this.storeService.saveToken(resp.token);
        }
        , err => {
          this.router.navigate(['login'], {queryParams: {returnUrl: this.router.url}});
        });
    }
  }

}
