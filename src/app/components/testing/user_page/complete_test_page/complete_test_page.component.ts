import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TestUserService} from '../../../../services/test_user.service';

@Component({
  selector: 'app-complete-test-page',
  templateUrl: 'complete_test_page.component.html',
  styleUrls: ['complete_test_page.component.scss']
})
export class CompleteTestPageComponent implements OnInit {

  testProcessId: number;

  constructor(public route: ActivatedRoute,
              public router: Router,
              public snackBar: MatSnackBar,
              public testUserService: TestUserService) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.testProcessId = Number(params.get('testProcessId'));
      this.testUserService.finishTest(this.testProcessId).subscribe(resp => {

      });
    });
  }


}
