import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TestUserService} from '../../../../services/test_user.service';
import {MatTableDataSource} from '@angular/material/table';
import {Test} from '../../../../models/test';
import {ActiveTestProcess, TestInAction} from '../../../../models/test-user';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-active-test-list',
  templateUrl: 'testlist.component.html',
  styleUrls: ['testlist.component.scss']
})


export class ActiveTestsComponent implements OnInit {

  pipe = new DatePipe('ru');
  activeTests: TestInAction[];

  constructor(public route: ActivatedRoute,
              public router: Router,
              public snackBar: MatSnackBar,
              public testUserService: TestUserService) {
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      const testId = Number(params.get('testId'));
      if (testId) {
        this.testUserService.getActiveTestProcess(testId).subscribe(resp => {
          this.activeTests = [];
          this.activeTests.push(resp);
          const activeTest = this.activeTests[0];
          if (activeTest) {
            if (!activeTest.complete && activeTest.testProcessList.length === 0) {
              this.initNew(activeTest.testId);
            }
          }
        });
      } else {
        this.testUserService.getAllActiveTestProcess().subscribe(resp => {
          this.activeTests = resp;
        });
      }
    });
  }

  initNew(testId: number) {
    this.router.navigate(['test_user/process'], {queryParams: {id: testId, action: 'new'}});
    // this.router.navigate(['test_user/process']);
  }

  continueTest(testProcessId: number) {
    this.router.navigate(['test_user/process'], {queryParams: {id: testProcessId, action: 'continue'}});
  }

}
