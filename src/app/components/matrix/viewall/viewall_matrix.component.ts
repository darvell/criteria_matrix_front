import {AfterViewChecked, AfterViewInit, Component, ElementRef, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {CriteriaMatrix} from '../../../models/matrix';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {StoreService} from '../../../services/store.service';
import {SubtitlePageComponent} from '../../main/page-with-subtitle/subtitle-page.component';

@Component({
  selector: 'app-view-all-matrix',
  templateUrl: './viewall_matrix.component.html',
  styleUrls: ['./viewall_matrix.component.scss']
})

export class ViewAllMatrixComponent extends SubtitlePageComponent implements OnInit, AfterViewChecked {

  public matrixes: CriteriaMatrix[];
  public showScrollUp = false;

  @ViewChild('container') elementView: ElementRef;

  constructor(private matriXService: MatrixAdminService,
              private router: Router,
              public snackBar: MatSnackBar,
              public storeService: StoreService) {
    super();
    this.subtitleActions.push({title: 'Добавить', actionId: 0});
  }

  ngOnInit(): void {
    this.storeService.changeLastToMatrix();
    this.matriXService.getAllMatrix().subscribe(
      resp => {
        this.matrixes = resp.sort((a, b) => b.id - a.id).filter(m => !m.deleted);
      }
    );
  }

  ngAfterViewChecked(): void {
    this.onResize();
  }

  onSubtitleAction(actionId: number) {
    switch (actionId) {
      case 0: {
        this.addNewMatrix();
        break;
      }
    }
  }

  addNewMatrix() {
    this.router.navigate(['/app/add']);
  }

  editMatrix(matrixId: number) {
    this.router.navigate(['/app/viewMatrix', matrixId]);
  }

  deleteMatrix(matrixId: number) {
    if (matrixId) {
      this.matriXService.deleteMatrix(matrixId).subscribe(resp => {
        this.matrixes = this.matrixes.filter(m => m.id !== matrixId);
        this.snackBar.open('Матрица удалена', 'Ок', {duration: 3000});
      });
    }
  }

  onResize() {
    this.showScrollUp = this.elementView.nativeElement.offsetHeight >= window.innerHeight;
  }

  scrollToUp() {
    window.scroll(0, 0);
  }


}
