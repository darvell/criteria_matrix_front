import {Component, Input} from '@angular/core';
import {Criteria, MatrixEntity} from '../../../../models/matrix';
import {FormArray, FormBuilder} from '@angular/forms';
import {MatrixAdminService} from '../../../../services/matrixadmin.service';

@Component({
  selector: 'app-entitys-editor',
  templateUrl: './entitys_editor.component.html'
})

export class EntitysEditorComponent {

  entitysToEdit: MatrixEntity[];
  newEntity: MatrixEntity = {id: -1, value: ''};

  constructor(private fb: FormBuilder,
              private matrixService: MatrixAdminService) {
  }

  @Input()
  set entitys(entities: MatrixEntity[]) {
    this.entitysToEdit = entities;
  }

  @Input()
  matrixId: number;

  update(entityId: number, entity: MatrixEntity) {
    if (entity == null) { return; }
    // copy and mutate
    this.entitysToEdit.find(c => c.id === entityId).value = entity.value;
    this.matrixService.updateEntityValue(entityId, entity.value).subscribe();
  }

  add(entity: MatrixEntity) {
    if (entity == null) { return; }
    this.matrixService.addEntity(this.matrixId, entity).subscribe(
      resp => {
        this.entitysToEdit.push(resp);
      }
    );
  }
}
