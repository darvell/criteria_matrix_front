import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';
import {Criteria, MatrixEntity} from '../../../../../models/matrix';
import {filter} from 'rxjs/operators';
import {validateWeightValidator} from '../inline_criteria_add/inline_criteria_add.component';

@Component({
  selector: 'app-inline-add-entity',
  templateUrl: './inline_entity_add.component.html',
  styleUrls: ['./inline_entity_add.component.scss']
})

export class InlineEntityAddComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private popover: SatPopover) {
  }

  mEntity: MatrixEntity;
  mIsAdd = false;

  entityAddfFG = this.fb.group({
    value: ['', Validators.required],
  });

  @Input()
  set add(isAdd: boolean) {
    this.mIsAdd = isAdd;
  }

  @Input()
  set entity(entity: MatrixEntity) {
    this.mEntity = entity;
    this.entityAddfFG.setValue({value: entity.value});
  }

  onSubmit() {
    if (this.popover) {
      if (this.entityAddfFG.valid) {
        const result = {
          value: this.entityAddfFG.get('value').value,
        };
        if (this.mIsAdd) {
          this.entityAddfFG.reset();
        }
        this.popover.close(result);
      }
    }
  }

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }

  ngOnInit(): void {
    if (this.popover) {
      this.popover.closed.pipe(filter(val => val == null))
        .subscribe(() => {
          if (this.mIsAdd) {
            this.entityAddfFG.get('value').setValue('');
          } else {
            this.entityAddfFG.get('value').setValue(this.mEntity.value);
          }
        });
    }
  }
}


