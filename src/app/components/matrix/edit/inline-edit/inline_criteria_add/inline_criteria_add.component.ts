import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';
import {Criteria} from '../../../../../models/matrix';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-inline-add-criteria',
  templateUrl: './inline_criteria_add.component.html',
  styleUrls: ['./inline_criteria_add.component.scss']
})

export class InlineCriteriaAddComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private popover: SatPopover) {
  }

  mHasWeight = false;
  mIsAdd = false;
  mCriteria: Criteria;

  criteriaAddfFG = this.fb.group({
    value: ['', Validators.required],
  });

  @Input()
  set add(isAdd: boolean) {
    this.mIsAdd = isAdd;
  }

  @Input()
  set hasWeight(weight: boolean) {
    this.mHasWeight = weight;
  }

  @Input()
  set criteria(criteria: Criteria) {
    this.mCriteria = criteria;
    this.criteriaAddfFG.setValue({value: criteria.value});
    if (this.mHasWeight) {
      this.criteriaAddfFG.addControl(
        'weight', this.fb.control(criteria.weight, [Validators.required, validateWeightValidator()])
      );

    }
  }


  onSubmit() {
    if (this.popover) {
      if (this.criteriaAddfFG.valid) {
        const result = {
          value: this.criteriaAddfFG.get('value').value,
          weight: this.mHasWeight ? this.criteriaAddfFG.get('weight').value : 0
        };
        if (this.mIsAdd) {
          this.criteriaAddfFG.reset();
        }
        this.popover.close(result);
      }
    }
  }

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }

  ngOnInit(): void {
    if (this.popover) {
      this.popover.closed.pipe(filter(val => val == null))
        .subscribe(() => {
          if (this.mIsAdd) {
            this.criteriaAddfFG.get('value').setValue('');
            if (this.mHasWeight) {
              this.criteriaAddfFG.get('weight').setValue(0);
            }
          } else {
            this.criteriaAddfFG.get('value').setValue(this.mCriteria.value || '');
            if (this.mHasWeight) {
              this.criteriaAddfFG.get('weight').setValue(this.mCriteria.weight || 0);
            }
          }
        });
    }
  }
}

export function validateWeightValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    return control.value <= 0 ? {valid: false} : null;
  };
}
