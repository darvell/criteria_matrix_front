import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';
import {filter, min} from 'rxjs/operators';

@Component({
  selector: 'app-inline-editor',
  templateUrl: './inlineeditor.component.html',
  styleUrls: ['./inlineeditor.component.scss']
})
export class InlineeditorComponent implements OnInit{

  constructor(private fb: FormBuilder,
              private popover: SatPopover) {
  }

  private mValue = '';
  mDigitalType = false;
  valueEdit = this.fb.control(['', Validators.required]);

  @Input()
  get value(): string {
    return this.mValue;
  }
  set value(v: string) {
    this.mValue = v;
    this.valueEdit.setValue(v);
  }

  @Input()
  set digitalType(d: boolean) {
    if (d) {
      this.mDigitalType = d;
      this.valueEdit = this.fb.control('', [Validators.required, Validators.min(1), Validators.max(10)]);
    }
  }

  ngOnInit() {
    if (this.popover) {
      this.popover.closed.pipe(filter(val => val == null))
        .subscribe(() => this.valueEdit.setValue(this.value || ''));
    }
  }

  onSubmit() {
    if (this.popover) {
      if (this.valueEdit.valid) {
        this.popover.close(this.valueEdit.value);
      }
    }
  }

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }


}
