import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CriteriaMatrix, Opros} from '../../../../models/matrix';
import {FormBuilder} from '@angular/forms';
import {MatrixAdminService} from '../../../../services/matrixadmin.service';
import {Router} from '@angular/router';
import {environment} from '../../../../../environments/environment';
import {Clipboard} from '@angular/cdk/clipboard';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-oproses-viewer',
  templateUrl: './oproses_viewer.component.html',
  styleUrls: ['oproses_viewer.component.scss']
})

export class OprosesViewerComponent {
  oprosesToView: Opros[];

  constructor(private fb: FormBuilder,
              private matrixService: MatrixAdminService,
              private router: Router,
              private clipboard: Clipboard,
              public snackBar: MatSnackBar) {
  }

  @Input()
  set oproses(oproses: Opros[]) {
    this.oprosesToView = oproses;
    console.log(new Date());
    oproses.forEach(o => o.createDate = new Date(o.createDate));
    oproses.sort((a, b) => b.createDate.getTime() - a.createDate.getTime());
  }

  @Output()
  updateAction = new EventEmitter<CriteriaMatrix>();

  public stopOpros(oprosId: number) {
    console.log(oprosId);
    this.matrixService.stopOpros(oprosId).subscribe(resp => this.updateAction.emit(resp));
  }

  public showQR(id: number) {
    window.open(`${environment.apiEndpoint}/qrcode/opros/${id}`, '_blank');
  }

  public showResult(id: number) {
    this.router.navigate(['/app/viewResult', id]);
  }

  public showXls(id: number) {
    this.matrixService.getExcelMatrix(id).subscribe(resp => {
      // const blob = new Blob([resp], {type: 'application/ms-excel'});
      // const url = window.URL.createObjectURL(blob);
      // const pwa = window.open(url);
      // if (!pwa || pwa.closed || typeof pwa.closed === 'undefined') {
      //   alert('Please disable your Pop-up blocker and try again.');
      // }

      const filename = 'matrix.xlsx';
      const dataType = resp.type;
      const binaryData = [];
      binaryData.push(resp);
      const downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
      if (filename) {
        downloadLink.setAttribute('download', filename);
      }
      document.body.appendChild(downloadLink);
      downloadLink.click();
    });
  }

  public copyLink(id: number) {
    this.clipboard.copy(`https://matrix.darvell.ru/opros/join/${id}`);
    this.snackBar.open('Ссылка скопирована', 'Ок', {duration: 3000});

  }

  refreshCount(oprosId: number) {
    this.matrixService.getInterviewersCount(oprosId).subscribe(
      resp => {

        console.log(resp);
        this.oprosesToView.find(o => o.id === oprosId).intervieweesCount = resp;
      }
    );
  }

  deleteOpros(oprosId: number) {
    this.matrixService.deleteOpros(oprosId).subscribe(resp => {
      this.snackBar.open('Опрос удалён', 'Ок', {duration: 3000});
      this.updateAction.emit(resp);
    });
  }

  // downLoadFile(data: any, type: string) {
  //   const fileName = 'file1.xlsx';
  //   const a = document.createElement('a');
  //   document.body.appendChild(a);
  //   a.style = 'display: none';
  //   const blob = new Blob([data], {type: type});
  //   const url = window.URL.createObjectURL(blob);
  //   a.href = url;
  //   a.download = fileName;
  //   a.click();
  //   window.URL.revokeObjectURL(url);
  // }
}
