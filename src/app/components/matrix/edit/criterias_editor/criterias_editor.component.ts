import {Component, Input} from '@angular/core';
import {Criteria} from '../../../../models/matrix';
import {FormArray, FormBuilder} from '@angular/forms';
import {MatrixAdminService} from '../../../../services/matrixadmin.service';

@Component({
  selector: 'app-criterias-editor',
  templateUrl: './criterias_editor.component.html',
  styleUrls: ['./criterias_editor.component.scss']
})

export class CriteriasEditorComponent {

  criteriasToEdit: Criteria[];
  mHasWeight = false;
  newCriteria: Criteria = {id: -1, value: '', weight: 0};

  constructor(private fb: FormBuilder,
              private matrixService: MatrixAdminService) {
  }

  @Input()
  set criterias(criterias: Criteria[]) {
    this.criteriasToEdit = criterias;
  }

  @Input()
  matrixId: number;

  @Input()
  set hasWeight(weight: boolean) {
    this.mHasWeight = weight;
  }

  update(critId: number, criteria: Criteria) {
    if (criteria == null) { return; }
    // copy and mutate
    const crit = this.criteriasToEdit.find(c => c.id === critId);
    crit.value = criteria.value;
    crit.weight = criteria.weight;
    // console.log(criteria);
    this.matrixService.updateCriteriaValue(critId, criteria.value, criteria.weight).subscribe();
  }

  add(criteria: Criteria) {
    if (criteria == null) { return; }
    // console.log(criteria);
    this.matrixService.addCriteria(this.matrixId, criteria).subscribe(
      resp => {
        this.criteriasToEdit.push(resp);
      }
    );
  }
}
