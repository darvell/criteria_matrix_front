import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {CriteriaMatrix} from '../../../models/matrix';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-matrix',
  templateUrl: './edit_matrix.component.html',
  styleUrls: ['edit_matrix.component.scss']
})
export class EditMatrixComponent implements OnInit {

  matrix: CriteriaMatrix;

  constructor(private route: ActivatedRoute,
              private matriXService: MatrixAdminService,
              private router: Router,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id');
        return this.matriXService.getMatrixById(Number(id));
      })
    ).subscribe(resp => {
      this.matrix = resp;
    });
  }

  public updateMatrix(data: CriteriaMatrix) {
    this.matrix = data;
  }

  public addOpros() {
    this.matriXService.newOpros(this.matrix.id).subscribe(resp => this.matrix = resp);
  }

  public back() {
    this.location.back();
  }
}
