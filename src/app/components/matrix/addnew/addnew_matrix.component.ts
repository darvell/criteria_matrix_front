import {Component} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {BulkDataFromInline} from './inline/bulk_criterias/inline_criteria_add_bulk.component';
import {CreateMatrixRequest} from '../../../models/matrix';

@Component({
  selector: 'app-add-new-matrix',
  templateUrl: './addnew_matrix.component.html',
  styleUrls: ['./addnew_matrix.component.scss']
})

export class AddNewMatrixComponent {

  isSaveError = false;

  matrixPrimaryForm = this.fb.group({
    name: ['', Validators.required],
    useWeight: [false],
  });

  criteriasGroup = this.fb.group({
    count: [0, Validators.min(1)],
    criterias: this.fb.array([])
  });

  entitysGroup = this.fb.group({
    count: [0, Validators.min(1)],
    entitys: this.fb.array([])
  });


  constructor(private fb: FormBuilder,
              private router: Router,
              private matriXService: MatrixAdminService) {
  }

  public cancelAndGoBack() {
    this.router.navigate(['/app/viewall']);
  }

  resetForm() {
    this.criterias.clear();
    this.entitys.clear();
    this.criteriasGroup.get('count').setValue(0);
    this.entitysGroup.get('count').setValue(0);
  }

  get criterias() {
    return this.criteriasGroup.get('criterias') as FormArray;
  }

  get entitys() {
    return this.entitysGroup.get('entitys') as FormArray;
  }

  get useWeight() {
    return this.matrixPrimaryForm.get('useWeight').value as boolean;
  }

  public plusCriteria() {
    this.criterias.push(this.fb.group({
      value: [''],
      weight: this.useWeight ? [0, Validators.required] : [0]
    }));
    this.criteriasGroup.get('count').setValue(this.criterias.length);
  }

  public minusCriteria() {
    if (this.criterias && this.criterias.length > 0) {
      this.criterias.removeAt(this.criterias.length - 1);
    }
    this.criteriasGroup.get('count').setValue(this.criterias.length);
  }

  public deleteCriteria(i: number) {
    this.criterias.removeAt(i);
    this.criteriasGroup.get('count').setValue(this.criterias.length);
  }

  public addBulkCriterias(cArr: BulkDataFromInline[]) {
    if (cArr) {
      for (const c of cArr) {
        this.criterias.push(this.fb.group({
          value: [c.value],
          weight: this.useWeight ? [c.weight, Validators.required] : [0]
        }));
      }
      this.criteriasGroup.get('count').setValue(this.criterias.length);
    }
  }

  public clearCriterias() {
    this.criterias.clear();
    this.criteriasGroup.get('count').setValue(this.criterias.length);
  }

  public plusEntity() {
    this.entitys.push(this.fb.group({
      value: [''],
    }));
    this.entitysGroup.get('count').setValue(this.entitys.length);
  }

  public minusEntity() {
    if (this.entitys && this.entitys.length > 0) {
      this.entitys.removeAt(this.entitys.length - 1);
    }
    this.entitysGroup.get('count').setValue(this.entitys.length);
  }

  public addBulkEntitys(cArr: BulkDataFromInline[]) {
    if (cArr) {
      for (const c of cArr) {
        this.entitys.push(this.fb.group({
          value: [c.value],
        }));
      }
      this.entitysGroup.get('count').setValue(this.entitys.length);
    }
  }

  public deleteEntity(i: number) {
    this.entitys.removeAt(i);
    this.entitysGroup.get('count').setValue(this.entitys.length);
  }

  public clearEntitys() {
    this.entitys.clear();
    this.entitysGroup.get('count').setValue(this.entitys.length);
  }

  public onSubmit() {
    console.log('submit');
    this.isSaveError = false;

    let i = 1;
    for (const crit of this.criteriasGroup.get('criterias').value) {
      if (crit.value === '') {
        crit.value = 'Критерий ' + i;
      }
      i++;
    }

    i = 1;
    for (const ent of this.entitysGroup.get('entitys').value) {
      if (ent.value === '') {
        ent.value = 'Сущность ' + i;
      }
      i++;
    }
    const request: CreateMatrixRequest = {
      name: this.matrixPrimaryForm.get('name').value,
      useCoefficient: this.matrixPrimaryForm.get('useWeight').value,
      entityList: this.entitysGroup.get('entitys').value,
      criteriaList: this.criteriasGroup.get('criterias').value
    };

    this.matriXService.createMatrix(
      request
    ).subscribe(
      resp => {
        this.router.navigate(['/app/viewall']);
      }, error => {
        this.isSaveError = true;
      }
    );
  }

}
