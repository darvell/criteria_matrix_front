import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';

@Component({
  selector: 'app-inline-add-criteria-bulk',
  templateUrl: './inline_criteria_add_bulk.component.html',
  styleUrls: ['./inline_criteria_add_bulk.component.scss']
})
export class InlineAddCriteriaBulkComponent {

  constructor(private fb: FormBuilder,
              private popover: SatPopover) {
  }

  mForm = this.fb.group({
      textFromBuffer: ['', Validators.required]
    }
  );

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }


  onSubmit() {
    if (this.popover && this.mForm.valid) {
      const result: BulkDataFromInline[] = [];
      const rawText: string = this.mForm.get('textFromBuffer').value;

      result.push(...rawText.split('\n')
        .filter(el => el !== '')
        .map<BulkDataFromInline>(el => new BulkDataFromInline(el))
      );
      this.popover.close(result);
    }
  }

}


export class BulkDataFromInline {
  value: string;
  weight: number;

  constructor(s: string) {
    const tArr = s.split('\t');
    if (tArr.length > 0) {
      this.value = tArr[0];
      this.weight = isNaN(Number(tArr[1])) ? 0 : Number(tArr[1]);
    } else {
      this.value = '';
      this.weight = 0;
    }
  }
}
