import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';

@Component({
  selector: 'app-inline-add-entity-bulk',
  templateUrl: './inline_entity_add_bulk.component.html',
  styleUrls: ['./inline_entity_add_bulk.component.scss']
})
export class InlineAddEntityBulkComponent {

  constructor(private fb: FormBuilder,
              private popover: SatPopover) {
  }

  mForm = this.fb.group({
      textFromBuffer: ['', Validators.required]
    }
  );

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }


  onSubmit() {
    if (this.popover && this.mForm.valid) {
      const result: BulkDataFromInline[] = [];
      const rawText: string = this.mForm.get('textFromBuffer').value;

      result.push(...rawText.split('\n')
        .filter(el => el !== '')
        .map<BulkDataFromInline>(el => new BulkDataFromInline(el))
      );
      this.popover.close(result);
    }
  }

}


export class BulkDataFromInline {
  value: string;
  weight: number;

  constructor(s: string) {
    this.value = s;
  }
}
