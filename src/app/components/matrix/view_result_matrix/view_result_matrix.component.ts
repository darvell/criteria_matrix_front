import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSort, Sort} from '@angular/material/sort';
import {FormBuilder} from '@angular/forms';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {MatTableDataSource} from '@angular/material/table';
import {DecimalPipe, Location} from '@angular/common';

@Component({
  selector: 'app-view-result-matrix',
  templateUrl: './view_result_matrix.component.html',
  styleUrls: ['./view_result_matrix.component.scss']
})
export class ViewResultMatrixComponent implements OnInit {

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private matrixService: MatrixAdminService,
              private fb: FormBuilder,
              private location: Location) {
  }

  private sort: MatSort;

  oprosId: number;
  // resultMatrix: Map<number, Map<number, number>>;
  criteriaLegend = new Map<string, string>();
  entityLegend = new Map<string, string>();
  weights = new Map<string, number>();

  // viewMatrix: [{[key: string]: any}];
  viewMatrix: any[];
  dataSource = new MatTableDataSource<any>();

  decimalPipe = new DecimalPipe('en-US');


  displayedColumns: string[];
  subheader: string[];

  noResultsError = false;
  dataFetching = false;

  setDataSourceAttributes() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.oprosId = Number(params.get('oprosId'));
      if (this.oprosId) {
        this.dataFetching = true;
        this.noResultsError = false;
        this.matrixService.getResultMatrixByOprosId(this.oprosId).subscribe(
          resp => {
            this.dataFetching = false;
            Object.keys(resp.entityLegend).forEach(key => {
              this.entityLegend.set(key, resp.entityLegend[key]);
            });

            Object.keys(resp.criteriaLegend).forEach(key => {
              this.criteriaLegend.set(key, resp.criteriaLegend[key]);
            });

            if (resp.weights) {
              Object.keys(resp.weights).forEach(key => {
                this.weights.set(key, resp.weights[key]);
              });
            }

            this.viewMatrix = [];
            for (const key of Object.keys(resp.resultMatrix)) {
              let tmp = {...resp.resultMatrix[key]};
              let complexCalc = 0;
              for (const inKey of Object.keys(tmp)) {
                let sum = tmp[inKey];
                if (resp.weights) {
                  sum *= this.weights.get(inKey);
                }
                complexCalc += sum;
                console.log(tmp[inKey]);
              }
              tmp = Object.assign({title: this.entityLegend.get(key), complex: complexCalc}, tmp);
              // console.log(tmp);
              this.viewMatrix.push(tmp);
            }

            this.prepareDisplayedColumns();
            this.dataSource = new MatTableDataSource<any>(this.viewMatrix);
          },
          error => {
            this.dataFetching = false;
            this.noResultsError = true;
          }
        );
      }
    });
  }

  prepareDisplayedColumns() {
    this.displayedColumns = [];
    this.displayedColumns.push('title');
    this.displayedColumns.push(...Array.from(this.criteriaLegend.keys()));
    this.displayedColumns.push('complex');

    this.subheader = [];
    this.subheader.push('weight');
    this.subheader.push(...Array.from(this.criteriaLegend.keys()).map(e => e + '_'));
    this.subheader.push('empty');
    // console.log(Array.from(this.weights.keys()).length);
  }

  showWeight() {
    return Array.from(this.weights.keys()).length > 0;
  }

  getFieldName(field: string): string {
    if (field === 'title') {
      return 'title';
    }
    if (field === 'complex') {
      return 'Комплексная оценка';
    }
    return this.criteriaLegend.get(field);
  }

  getSubHeader(field: string): string {
    if (field === 'weight') {
      return 'вес';
    } else if (field === 'empty') {
      return '';
    } else {
      const s = field.substr(0, field.length - 1);
      return this.decimalPipe.transform(this.weights.get(s), '1.0-2');
    }
  }

  getFieldDataFormatted(field: string, value: any): string {
    if (field === 'title') {
      return value;
    }
    return this.decimalPipe.transform(value, '1.0-2');
  }

  getDataStyle(field: string): any {
    if (field !== 'title') {
      // return 'text-align: center; border: 1px solid; border-style :   none none none solid';
      return 'text-align: center; padding-right: 0';
    } else {
      return 'padding-left: 10px; padding-right: 10px';
    }
  }

  back() {
    this.location.back();
  }

}
