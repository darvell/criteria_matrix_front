import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {StoreService} from '../../services/store.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login_page.component.html',
  styleUrls: ['./login_page.component.scss']
})

export class LoginPageComponent implements OnInit {

  login = new FormControl();
  password = new FormControl();
  isErrorLogin = false;

  returnUrl: string;

  constructor(private authService: AuthService,
              private storeService: StoreService,
              private router: Router,
              public route: ActivatedRoute,) {
  }

  public onLoginClick() {
    this.isErrorLogin = false;
    this.authService.getAuth(this.login.value, this.password.value).subscribe(resp => {
      this.storeService.saveToken(resp.token);
      if (this.returnUrl) {
        this.router.navigateByUrl(this.returnUrl);
      } else {
        this.router.navigate(['app/']);
      }
    }, err => {
      if (err.status === 404) {
        this.isErrorLogin = true;
      }
    });
  }

  public onRegisterClick() {
    this.router.navigate(['register']);
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.returnUrl = params.get('returnUrl');
    });
  }

}
