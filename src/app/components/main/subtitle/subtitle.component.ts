import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SubtitleActionData} from '../../../models/component_models';


@Component({
  selector: 'app-subtitle',
  templateUrl: 'subtitle.component.html',
  styleUrls: ['subtitle.component.scss']
})
export class SubtitleComponent implements OnInit{

  @Input()
  showBackAction = false;

  @Input()
  title: string;

  @Input()
  actions: SubtitleActionData[];

  @Output()
  actionListener: EventEmitter<number> = new EventEmitter();

  @Output()
  backAction: EventEmitter<void> = new EventEmitter();



  ngOnInit(): void {
    if (this.actions) {
      this.actions.forEach(action => console.log(action));
    }
    console.log(this.showBackAction);
  }

  actionClick(actionId: number) {
    this.actionListener.emit(actionId);
  }

}
