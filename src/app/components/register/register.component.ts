import {Component} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {StoreService} from '../../services/store.service';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterPageComponent {
  login = new FormControl('', Validators.required);
  password = new FormControl();
  password2 = new FormControl();

  isErrorLogin = false;
  passwordError = false;

  constructor(private authService: AuthService,
              private storeService: StoreService,
              private router: Router) {
  }

  public onRegisterClick() {
    this.isErrorLogin = false;
    this.passwordError = false;
    if (!this.login.valid) {
      return;
    }
    if (this.password.value !== this.password2.value) {
      this.passwordError = true;
      return;
    }
    this.authService.register(this.login.value, this.password.value).subscribe(resp => {
      this.storeService.saveToken(resp.token);
      this.router.navigate(['app/']);
    }, err => {
      if (err.status === 404) {
        this.isErrorLogin = true;
      }
    });
  }

  public onBackClick() {
    this.router.navigate(['login/']);
  }
}
