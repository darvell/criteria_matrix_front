import {AfterContentInit, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {OprosService} from '../../../services/opros.service';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {Criteria, MatrixEntity} from '../../../models/matrix';
import {Interview, JoinOprosResponse, MergedInterview, OprosAnswer} from '../../../models/opros';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {MatTableDataSource} from '@angular/material/table';
import {MatSliderChange} from '@angular/material/slider';
import {MatStepper} from '@angular/material/stepper';
import {MatSnackBar} from '@angular/material/snack-bar';
import {JoinOprosComponent} from './join_opros.component';
import {Observable} from 'rxjs';

@Component({
  templateUrl: './join_opros.component.html',
  styleUrls: ['./join_opros.component.scss']
})
export class JoinMergedOprosComponent extends JoinOprosComponent {

  interviews: Interview[];
  currInterviewIndex = 0;

  submitFio() {
    this.oprosService.sendFioToMerged(this.interviewId, this.fioForm.value).subscribe(
      resp => {
        this.interviews = resp.interviews;
        const currInterview = this.interviews[this.currInterviewIndex];
        // console.log(currInterview);
        this.loadInterview(currInterview);
        this.interviewId = currInterview.interviewId;
      }
    );
  }

  doExtra() {
    if (this.isOprosEnded) {
      this.currInterviewIndex++;
      if (this.currInterviewIndex < this.interviews.length) {
        this.isOprosEnded = false;
        const currInterview = this.interviews[this.currInterviewIndex];
        this.loadInterview(currInterview);
        this.interviewId = currInterview.interviewId;
      }
    }
  }

  execJoinAndCheck(oprosId: number): Observable<JoinOprosResponse> {
    return this.oprosService.joinAndCheckMerged(oprosId);
  }
}
