import {AfterContentInit, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {MatrixAdminService} from '../../../services/matrixadmin.service';
import {OprosService} from '../../../services/opros.service';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {Criteria, EntityGroup, MatrixEntity} from '../../../models/matrix';
import {Interview, JoinOprosResponse, OprosAnswer} from '../../../models/opros';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {MatTableDataSource} from '@angular/material/table';
import {MatSliderChange} from '@angular/material/slider';
import {MatStepper} from '@angular/material/stepper';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {LastInterviwee, StoreService} from '../../../services/store.service';

@Component({
  templateUrl: './join_opros.component.html',
  styleUrls: ['./join_opros.component.scss']
})
export class JoinOprosComponent implements OnInit {

  @ViewChild('stepper') myStepper: MatStepper;

  oprosId: number;
  interviewId: number;
  merged = false;

  isShowContiinueWindow = false;

  isOprosOpen = true;
  isOprosInProcess = false;
  isOprosEnded = false;

  isNeedShowMin = true;
  isNeedShowMax = true;
  isPartAnswer = false;
  maxRating = 10;
  maxAnswersCount = 0;
  custom = 0;

  answerType = 'slider';
  actionText = 'Добавьте недостающие оценки';

  testText = 'Оценка';

  errorRating: number[] = [1, 2, 3, 4, 5];
  uniqError = false;

  criterias: Criteria[];
  entitys: MatrixEntity[];
  groups: EntityGroup[];

  storedInterviewee = new LastInterviwee();

  currCriteriaIndex = 0;

  entitysWithoutMax: MatrixEntity[] = [];

  showSendAnswerError = false;


  fioForm = this.fb.group({
    f: ['', Validators.required],
    i: ['', Validators.required],
    o: ['', Validators.required],
  });

  tableData: OprosAnswer[];
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[];


  maxFG = this.fb.group({
    maxEntitySelected: ['', Validators.required],
  });

  minFG = this.fb.group(
    {
      minEntitySelected: ['', Validators.required],
    }
  );

  tableValidControl = this.fb.control(false, Validators.requiredTrue);

  constructor(public route: ActivatedRoute,
              public router: Router,
              public oprosService: OprosService,
              public fb: FormBuilder,
              public snackBar: MatSnackBar,
              public storeService: StoreService) {

    this.displayedColumns = ['entityName', 'answer'];
  }

  changeMinMaxSub() {
    if (this.isNeedShowMax) {
      this.maxFG.valueChanges.subscribe(val => {
          this.entitysWithoutMax = this.entitys.filter(e => e.id !== val.maxEntitySelected);
          this.minFG.reset();
          console.log('change max');
          if (!this.isNeedShowMin) {
            this.fillMinMaxInAnswers();
          } else {
            this.minFG.valueChanges.subscribe(() => this.fillMinMaxInAnswers());
          }
        }
      );
    } else if (this.isNeedShowMin) {
      this.entitysWithoutMax = this.entitys;
      this.minFG.valueChanges.subscribe(() => this.fillMinMaxInAnswers());
    }
    // this.onSelectionChange(new StepperSelectionEvent());
  }


  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.oprosId = Number(params.get('oprosId'));
      if (this.oprosId) {
        this.oprosService.isOprosOpen(this.oprosId).subscribe(() => {
          },
          () => this.isOprosOpen = false);

        this.storedInterviewee = this.storeService.getOprosInfo(this.oprosId);
        if (this.storedInterviewee.interviewId !== 0 && !this.storedInterviewee.finished) {
          this.doPrepareContinue();
        } else {
          this.doNewInterviewActions();
        }
      }
    });
  }

  doNewInterviewActions() {
    this.isShowContiinueWindow = false;
    this.execJoinAndCheck(this.oprosId).subscribe(
      resp => {
        this.isOprosOpen = resp.success;
        this.interviewId = resp.interviewId;
        this.merged = resp.merged;
        console.log(this.interviewId);
      }
    );
  }

  execJoinAndCheck(oprosId: number): Observable<JoinOprosResponse> {
    return this.oprosService.joinAndCheck(oprosId);
  }

  public submitFio() {
    console.log('send fio');
    this.oprosService.sendFio(this.interviewId, this.fioForm.value).subscribe(
      resp => {
        this.storeService.setOprosInfo(this.oprosId, this.interviewId, false);
        this.loadInterview(resp);
      }
    );
  }

  public doPrepareContinue() {
    this.interviewId = this.storedInterviewee.interviewId;
    this.oprosService.canContinue(this.storedInterviewee.interviewId).subscribe(
      resp => {
        this.isShowContiinueWindow = true;
      }, error => {
        this.doNewInterviewActions();
      }
    );
  }

  public continueInterview() {
    this.oprosService.continueInterview(this.storedInterviewee.interviewId).subscribe(
      resp => {
        this.loadInterview(resp);
      }
    );
  }

  loadInterview(interview: Interview) {
    console.log('load interview');
    this.criterias = interview.criterias;
    this.entitys = interview.entitys;
    this.groups = interview.entityGroups;
    this.isNeedShowMax = interview.needMax;
    this.isNeedShowMin = interview.needMin;
    this.isPartAnswer = interview.partAnswer;
    this.maxRating = interview.maxRaiting;
    this.maxAnswersCount = interview.maxAnswersCount;
    if (interview.actionText) {
      this.actionText = interview.actionText;
    }
    if (interview.custom === 1) {
      this.testText = 'Приоритет прохождения <br/> 1 - важно <br/> 5 -менее важно';
    }
    this.custom = interview.custom;
    this.doFillMatrix();
    this.changeMinMaxSub();
    this.fillCriteriaOpros(0);
  }

  fillCriteriaOpros(i: number) {
    this.isOprosInProcess = true;
    this.currCriteriaIndex = i;
  }

  displayFn(entity: MatrixEntity) {
    return entity && entity.value ? entity.value : '';
  }

  public doFillMatrix() {
    this.tableData = [];

    this.entitys.forEach(e => {
      this.tableData.push(new OprosAnswer(e));
    });

    if (this.groups && this.groups.length > 0) {
      for (const g of this.groups) {
        this.tableData.filter(e => g.entityIds.includes(e.entityId)).forEach(e => e.groupId = g.id);
      }
      this.groups.push({id: -1, name: 'Без группы', entityIds: []});
    }

    this.dataSource = new MatTableDataSource<any>(this.tableData);
  }

  getAnswersByGroup(groupId: number): OprosAnswer[] {
    return this.tableData.filter(a => a.groupId === groupId);
  }

  fillMinMaxInAnswers() {
    console.log(this.tableData);
    if (this.tableData) {
      this.resetAnswers();
      if (this.isNeedShowMax) {
        if (this.maxFG.valid) {
          const maxEntSelected = this.maxFG.get('maxEntitySelected').value;
          this.tableData.find(e => e.entityId === maxEntSelected).answer = this.maxRating;
        }
      }

      if (this.isNeedShowMin) {
        if (this.minFG.valid) {
          const minEntSelected = this.minFG.get('minEntitySelected').value;
          console.log('minEntSelected  ' + minEntSelected);
          this.tableData.find(e => e.entityId === minEntSelected).answer = 1;
        }
      }
    }
  }

  resetAnswers() {
    this.tableData.forEach(e => {
      e.answer = 0;
      e.disabled = false;
      e.error = false;
    });
  }

  revalidateAnswerTable() {
    if (this.maxAnswersCount > 0) {
      if (this.tableData.filter(a => a.answer !== 0).length === this.maxAnswersCount) {
        this.tableData.filter(a => a.answer === 0).forEach(el => el.disabled = true);
      } else {
        this.tableData.forEach(el => el.disabled = false);
      }
    }

    if (this.isPartAnswer) {
      this.tableValidControl.setValue(true);
    } else {
      if (this.maxAnswersCount > 0) {
        if (this.tableData.filter(a => a.answer !== 0).length === this.maxAnswersCount) {
          this.tableValidControl.setValue(true);
        } else {
          this.tableValidControl.setValue(false);
        }
      } else {
        if (this.tableData.filter(a => a.answer === 0).length === 0) {
          this.tableValidControl.setValue(true);
        } else {
          this.tableValidControl.setValue(false);
        }
      }
    }

    if (this.custom === 1) {
      this.uniqError = false;
      this.tableData.forEach(e => {
        e.error = false;
      });
      for (const i of this.errorRating) {
        if (this.tableData.filter(el => el.answer === i).length > 1) {
          this.tableData.filter(el => el.answer === i).forEach(el => el.error = true);
          this.tableValidControl.setValue(false);
          this.uniqError = true;
        }
      }
    }
  }

  public nextCriteria() {
    this.oprosService.sendAnswers(this.interviewId, this.criterias[this.currCriteriaIndex].id, this.tableData)
      .subscribe(resp => {
          this.myStepper.reset();
          this.currCriteriaIndex += 1;
          this.maxFG.reset();
          this.minFG.reset();
          this.resetAnswers();
          this.showSendAnswerError = false;
          if (this.currCriteriaIndex === this.criterias.length) {
            this.isOprosEnded = true;
            this.storeService.setOprosInfo(this.oprosId, this.interviewId, true);
            this.doExtra();
          }
        },
        err => {
          console.error(err);
          this.showSendAnswerError = true;
        });
  }

  public doExtra() {
  }

  public onSelectionChange(event: StepperSelectionEvent) {
  }

  updateAnswer(id: number, event: MatSliderChange) {
    if (event == null) {
      return;
    }
    this.tableData.find(a => a.entityId === id).answer = event.value;
    this.revalidateAnswerTable();
  }

  plusClick(id: number) {
    const answ = this.tableData.find(a => a.entityId === id);

    if (answ.answer + 1 > this.maxRating) {
      answ.answer = 0;
    } else {
      answ.answer++;
    }
    this.revalidateAnswerTable();
  }

  minusClick(id: number) {
    const answ = this.tableData.find(a => a.entityId === id);

    if (answ.answer - 1 < 0) {
      answ.answer = this.maxRating;
    } else {
      answ.answer--;
    }
    this.revalidateAnswerTable();
  }


  onClickAnswerTableStep() {
    this.revalidateAnswerTable();
    if (this.tableValidControl.valid) {
      this.myStepper.next();
    } else {
      if (this.maxAnswersCount === 0) {
        this.snackBar.open('Заполните все неподсвеченные поля', 'Ок', {duration: 3000});
      } else {
        if (this.uniqError) {
          this.snackBar.open('Оценки должны быть уникальными', 'Ок', {duration: 3000});
        } else {
          const filledAnswers = this.tableData.filter(el => el.answer !== 0).length;
          this.snackBar.open('Заполните ещё ' + String(this.maxAnswersCount - filledAnswers), 'Ок', {duration: 3000});
        }
      }
    }
  }

}
