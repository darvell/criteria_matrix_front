import {Component, Input} from '@angular/core';
import {SatPopover} from '@ncstate/sat-popover';
import {BulkDataFromInline} from '../matrix/addnew/inline/bulk_entitys/inline_entity_add_bulk.component';

@Component({
  selector: 'app-delete-submit',
  templateUrl: './delete_submit.component.html',
  styleUrls: ['./delete_submit.component.scss']
})

export class DeleteSubmitComponent {
  constructor(private popover: SatPopover) {
  }

  @Input()
  text: string;

  @Input()
  id: number;

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }


  onSubmit() {
    if (this.popover) {
      this.popover.close(this.id);
    }
  }
}
