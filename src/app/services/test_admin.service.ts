import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QuestionRA, Test, TestQuestion, TestsResponse} from '../models/test';

@Injectable({
  providedIn: 'root'
})

export class TestAdminService {

  constructor(private http: HttpClient) {
  }

  public getAllTests(): Observable<TestsResponse> {
    return this.http.get<TestsResponse>(`${environment.apiEndpoint}/testing/all`);
  }

  public getTestById(id: number): Observable<Test> {
    return this.http.get<Test>(`${environment.apiEndpoint}/testing/${id}`);
  }

  public updateTest(id: number, test: Test): Observable<Test> {
    return this.http.post<Test>(`${environment.apiEndpoint}/testing/${id}`, test);
  }

  public addTest(test: Test): Observable<Test> {
    return this.http.post<Test>(`${environment.apiEndpoint}/testing/`, test);
  }

  public getQuestionsByTest(testId: number): Observable<TestQuestion[]> {
    return this.http.get<TestQuestion[]>(`${environment.apiEndpoint}/testing/${testId}/questions`);
  }

  public getQuestionById(questionId: number): Observable<QuestionRA> {
    return this.http.get<QuestionRA>(`${environment.apiEndpoint}/testing/questions/${questionId}`);
  }

  public addQuestion(testId: number, question: QuestionRA): Observable<QuestionRA> {
    return this.http.post<QuestionRA>(`${environment.apiEndpoint}/testing/${testId}/questions`, question);
  }

  public updateQuestion(questionId: number, question: QuestionRA): Observable<QuestionRA> {
    return this.http.post<QuestionRA>(`${environment.apiEndpoint}/testing/questions/${questionId}`, question);
  }

  public deleteTest(testId: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiEndpoint}/testing/${testId}`);
  }

  public deleteAnswer(answerId: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiEndpoint}/testing/answer/${answerId}`);
  }

}
