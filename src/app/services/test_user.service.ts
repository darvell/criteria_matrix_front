import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TestsResponse} from '../models/test';
import {environment} from '../../environments/environment';
import {ActiveTestProcess, TestInAction, TestProcess, TestToDo} from '../models/test-user';

@Injectable({
  providedIn: 'root'
})
export class TestUserService {

  constructor(private http: HttpClient) {
  }

  public joinTest(testId: number): Observable<TestProcess> {
    return this.http.get<TestProcess>(`${environment.apiEndpoint}/do_test/${testId}/join`);
  }

  public getTest(testProcessId: number): Observable<TestToDo> {
    return this.http.get<TestToDo>(`${environment.apiEndpoint}/do_test/${testProcessId}`);
  }

  public addAnswer(testProcessId: number, questionId: number, answers: number[]): Observable<any> {
    return this.http.post<any>(`${environment.apiEndpoint}/do_test/${testProcessId}/${questionId}`, answers);
  }

  public getActiveTestProcess(testId: number): Observable<TestInAction> {
    return this.http.get<TestInAction>(`${environment.apiEndpoint}/do_test/${testId}/active`);
  }

  public getAllActiveTestProcess(): Observable<TestInAction[]> {
    return this.http.get<TestInAction[]>(`${environment.apiEndpoint}/do_test/active`);
  }

  public finishTest(testId: number): Observable<void> {
    return this.http.get<void>(`${environment.apiEndpoint}/do_test/${testId}/finish`);
  }

}
