import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {LoginResponse, TokenCheckResponse} from '../models/auth_models';

const authPrefix = '/auth';
const registerPrefix = '/register';
const adminPrefix = '/admin';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  public getAuth(login: string, password: string): Observable<LoginResponse> {
    const formData: FormData = new FormData();
    formData.append('login', login);
    formData.append('password', password);
    return this.http.post<LoginResponse>(`${environment.apiEndpoint}${authPrefix}`, formData);
  }

  public register(login: string, password: string): Observable<LoginResponse> {
    const formData: FormData = new FormData();
    formData.append('login', login);
    formData.append('password', password);
    return this.http.post<LoginResponse>(`${environment.apiEndpoint}${registerPrefix}`, formData);
  }

  public printHello(){
    console.log('Hello!');
  }

  public checkToken(): Observable<TokenCheckResponse> {
    return this.http.post<TokenCheckResponse>(`${environment.apiEndpoint}/token/check`, null);
  }

  // public getUsers(): Observable<UserForEdit[]> {
  //   return this.http.get<UserForEdit[]>(`${environment.apiEndpoint}${authPrefix}${adminPrefix}/users`);
  // }
  //
  // public getAllUserRoles(): Observable<UserRole[]> {
  //   return this.http.get<UserRole[]>(`${environment.apiEndpoint}${authPrefix}${adminPrefix}/users/roles`);
  // }
  //
  //
  // public addUser(user: UserForEdit): Observable<UserForEdit> {
  //   return this.http.post<UserForEdit>(`${environment.apiEndpoint}${authPrefix}${adminPrefix}/users`, user);
  // }
  //
  // public updateUser(userId: number, user: UserForEdit): Observable<UserForEdit> {
  //   return this.http.put<UserForEdit>(`${environment.apiEndpoint}${authPrefix}${adminPrefix}/users/${userId}`, user);
  // }
}
