import {Injectable} from '@angular/core';

export const TOKEN_KEY = 'TOKEN';
export const OPROS_PREFIX = 'OPROS_';
export const OPROS_FINISH_PREFIX = 'OPROS_FINISH_';
export const LAST_SERVICE = 'last_service';
export const SERVICE_MATRIX = '1';
export const SERVICE_TESTING = '2';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public saveToken(token: string) {
    localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public deleteToken() {
    localStorage.removeItem(TOKEN_KEY);
  }

  public getOprosInfo(oprosId: number): LastInterviwee {
    const result = new LastInterviwee();
    result.interviewId = Number(localStorage.getItem(OPROS_PREFIX + String(oprosId)));
    result.finished = Boolean(localStorage.getItem(OPROS_FINISH_PREFIX + String(oprosId)));
    return result;
  }

  public setOprosInfo(oprosId: number, interviewId: number, finished: boolean) {
    localStorage.setItem(OPROS_PREFIX + String(oprosId), String(interviewId));
    if (finished) {
      localStorage.setItem(OPROS_FINISH_PREFIX + String(oprosId), String(finished));
    } else {
      localStorage.removeItem(OPROS_FINISH_PREFIX + String(oprosId));
    }
  }

  public changeLastToTesting() {
    localStorage.setItem(LAST_SERVICE, SERVICE_TESTING);
  }

  public changeLastToMatrix() {
    localStorage.setItem(LAST_SERVICE, SERVICE_MATRIX);
  }

  public isLastServiceTesting(): boolean {
    return localStorage.getItem(LAST_SERVICE) === SERVICE_TESTING;
  }
}


export class LastInterviwee {
  interviewId: number;
  finished: boolean;
}
