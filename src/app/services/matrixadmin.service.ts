import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CreateMatrixRequest, Criteria, CriteriaMatrix, MatrixEntity, ResultMatrixResp} from '../models/matrix';
import {environment} from '../../environments/environment';

const matrixPrefix = '/matrixAdmin';
const oprosPrefix = '/opros';

@Injectable({
  providedIn: 'root'
})

export class MatrixAdminService {
  constructor(private http: HttpClient) {
  }

  public getAllMatrix(): Observable<CriteriaMatrix[]> {
    return this.http.get<CriteriaMatrix[]>(`${environment.apiEndpoint}${matrixPrefix}`);
  }

  public getMatrixById(matrixId: number): Observable<CriteriaMatrix> {
    return this.http.get<CriteriaMatrix>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}`);
  }

  public createMatrix(request: CreateMatrixRequest): Observable<CriteriaMatrix> {
    return this.http.post<CriteriaMatrix>(`${environment.apiEndpoint}${matrixPrefix}`, request);
  }

  public deleteMatrix(matrixId: number): Observable<any> {
    return this.http.delete(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}`);
  }

  public addCriteria(matrixId: number, c: Criteria): Observable<Criteria> {
    const formData: FormData = new FormData();
    formData.append('value', c.value);
    formData.append('weight', String(c.weight));
    return this.http.post<Criteria>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}/add/criteria`, formData);
  }

  public addEntity(matrixId: number, e: MatrixEntity): Observable<Criteria> {
    const formData: FormData = new FormData();
    formData.append('value', e.value);
    return this.http.post<Criteria>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}/add/entity`, formData);
  }

  public addCriterias(matrixId: number, criterias: Criteria[]): Observable<CriteriaMatrix> {
    return this.http.post<CriteriaMatrix>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}/criteria`, criterias);
  }

  public addEntitys(matrixId: number, entitys: MatrixEntity[]): Observable<CriteriaMatrix> {
    return this.http.post<CriteriaMatrix>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}/entity`, entitys);
  }

  public stopOpros(oprosId: number): Observable<CriteriaMatrix> {
    return this.http.get<CriteriaMatrix>(`${environment.apiEndpoint}${oprosPrefix}/${oprosId}/stop`);
  }


  public newOpros(matrixId: number): Observable<CriteriaMatrix> {
    return this.http.get<CriteriaMatrix>(`${environment.apiEndpoint}${matrixPrefix}/${matrixId}/new_opros`);
  }

  public getResultMatrixByOprosId(oprosId: number): Observable<ResultMatrixResp> {
    return this.http.get<ResultMatrixResp>(`${environment.apiEndpoint}${oprosPrefix}/${oprosId}/get_result`);
  }

  public updateCriteriaValue(criteriaId: number, value: string, weight?: number): Observable<void> {
    const formData: FormData = new FormData();
    formData.set('value', value);
    if (weight) {
      formData.set('weight', String(weight));
    }
    return this.http.put<void>(`${environment.apiEndpoint}${matrixPrefix}/criteria/${criteriaId}`, formData);
  }

  public updateEntityValue(entityId: number, value: string): Observable<void> {
    const formData: FormData = new FormData();
    formData.set('value', value);
    return this.http.put<void>(`${environment.apiEndpoint}${matrixPrefix}/entity/${entityId}`, formData);
  }

  getExcelMatrix(oprosId: number): Observable<any> {
    return this.http.get(`${environment.apiEndpoint}${oprosPrefix}/${oprosId}/get_result/xls`,
      {responseType: 'arraybuffer'});
  }

  getInterviewersCount(oprosId: number): Observable<number> {
    return this.http.get<number>(`${environment.apiEndpoint}${oprosPrefix}/${oprosId}/interviewCount`);
  }

  deleteOpros(oprosId: number): Observable<CriteriaMatrix> {
    return this.http.delete<CriteriaMatrix>(`${environment.apiEndpoint}${oprosPrefix}/${oprosId}`);
  }

  // public getAllUserRoles(): Observable<UserRole[]> {
  //   return this.http.get<UserRole[]>(`${environment.apiEndpoint}${authPrefix}${adminPrefix}/users/roles`);
  // }

}
