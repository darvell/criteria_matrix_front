import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Interview, JoinOprosResponse, MergedInterview} from '../models/opros';
import {environment} from '../../environments/environment';

const interviewPrefix = '/interview';


@Injectable({
  providedIn: 'root'
})


export class OprosService {
  constructor(private http: HttpClient) {
  }

  public joinAndCheck(oprosId: number): Observable<JoinOprosResponse> {
    return this.http.get<JoinOprosResponse>(`${environment.apiEndpoint}${interviewPrefix}/opros/${oprosId}/join`);
  }

  public joinAndCheckMerged(oprosId: number): Observable<JoinOprosResponse> {
    return this.http.get<JoinOprosResponse>(`${environment.apiEndpoint}${interviewPrefix}/opros/merged/${oprosId}/join`);
  }

  public sendFio(interviewId: number, data: any): Observable<Interview> {
    const formData: FormData = new FormData();
    formData.append('f', data.f);
    formData.append('i', data.i);
    formData.append('o', data.o);
    return this.http.post<Interview>(`${environment.apiEndpoint}${interviewPrefix}/${interviewId}/fio`, formData);
  }
  public continueInterview(interviewId: number): Observable<Interview> {
    return this.http.get<Interview>(`${environment.apiEndpoint}${interviewPrefix}/${interviewId}/continue`);
  }

  public sendFioToMerged(interviewId: number, data: any): Observable<MergedInterview> {
    const formData: FormData = new FormData();
    formData.append('f', data.f);
    formData.append('i', data.i);
    formData.append('o', data.o);
    return this.http.post<MergedInterview>(`${environment.apiEndpoint}${interviewPrefix}/merged/${interviewId}/fio`, formData);

  }

  public sendAnswers(interviewId: number, criteriaId: number, answers: any): Observable<void> {
    return this.http.post<void>(`${environment.apiEndpoint}${interviewPrefix}/${interviewId}/criteria/${criteriaId}/answers`,
      answers);
  }

  public canContinue(interviewId: number): Observable<void>{
    return this.http.get<void>(`${environment.apiEndpoint}${interviewPrefix}/${interviewId}/can_continue`);
  }

  public isOprosOpen(oprosId: number): Observable<void>{
    return this.http.get<void>(`${environment.apiEndpoint}${interviewPrefix}/opros/${oprosId}/is_open`);
  }
}
