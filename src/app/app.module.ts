import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginPageComponent} from './components/auth/login_page.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MyOwnCustomMaterialModule} from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MainPageComponent} from './components/main_page/mainpage.component';
import {AuthenticationInterceptor} from './interceptors/auth_interceptor';
import {ViewAllMatrixComponent} from './components/matrix/viewall/viewall_matrix.component';
import {NgScrollbarModule} from 'ngx-scrollbar';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {AddNewMatrixComponent} from './components/matrix/addnew/addnew_matrix.component';
import {EditMatrixComponent} from './components/matrix/edit/edit_matrix.component';
import {CriteriasEditorComponent} from './components/matrix/edit/criterias_editor/criterias_editor.component';
import {OprosesViewerComponent} from './components/matrix/edit/oproses_viewer/oproses_viewer.component';
import {JoinOprosComponent} from './components/opros/join/join_opros.component';
import {ViewResultMatrixComponent} from './components/matrix/view_result_matrix/view_result_matrix.component';
import {MatSortModule} from '@angular/material/sort';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {InlineeditorComponent} from './components/matrix/edit/inline-edit/inlineeditor.component';
import {EntitysEditorComponent} from './components/matrix/edit/entitys_editor.component/entitys_editor.component';
import {MatSelectModule} from '@angular/material/select';
import {InlineCriteriaAddComponent} from './components/matrix/edit/inline-edit/inline_criteria_add/inline_criteria_add.component';
import {InlineEntityAddComponent} from './components/matrix/edit/inline-edit/inline_entity_add/inline_entity_add.component';
import {
  InlineAddCriteriaBulkComponent
} from './components/matrix/addnew/inline/bulk_criterias/inline_criteria_add_bulk.component';
import {InlineAddEntityBulkComponent} from './components/matrix/addnew/inline/bulk_entitys/inline_entity_add_bulk.component';
import {DeleteSubmitComponent} from './components/dialogs/delete_submit.component';
import {RegisterPageComponent} from './components/register/register.component';
import {JoinMergedOprosComponent} from './components/opros/join/join_merged_opros.component';
import {TestAdminComponent} from './components/testing/admin_page/testadmin.component';
import {TestEditComponent} from './components/testing/admin_page/test_edit/testedit.component';
import {QuestionEditComponent} from './components/testing/admin_page/question_edit/questionedit.component';
import {TestUserComponent} from './components/testing/user_page/testuser.component';
import {ActiveTestsComponent} from './components/testing/user_page/active_test_list/testlist.component';
import {registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import {TestingMainComponent} from './components/testing/user_page/main/testingmain.component';
import {SubtitleComponent} from './components/main/subtitle/subtitle.component';
import {CompleteTestPageComponent} from './components/testing/user_page/complete_test_page/complete_test_page.component';

registerLocaleData(localeRu, 'ru');

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    MainPageComponent,
    ViewAllMatrixComponent,
    AddNewMatrixComponent,
    EditMatrixComponent,
    CriteriasEditorComponent,
    OprosesViewerComponent,
    JoinOprosComponent,
    ViewResultMatrixComponent,
    InlineeditorComponent,
    EntitysEditorComponent,
    InlineCriteriaAddComponent,
    InlineEntityAddComponent,
    InlineAddCriteriaBulkComponent,
    InlineAddEntityBulkComponent,
    DeleteSubmitComponent,
    RegisterPageComponent,
    JoinMergedOprosComponent,
    TestAdminComponent,
    TestEditComponent,
    QuestionEditComponent,
    TestUserComponent,
    ActiveTestsComponent,
    TestingMainComponent,
    SubtitleComponent,
    CompleteTestPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyOwnCustomMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgScrollbarModule.withConfig({
      visibility: 'hover'
    }),
    ScrollingModule,
    FormsModule,
    MatSortModule,
    SatPopoverModule,
    MatSelectModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
